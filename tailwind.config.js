/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontWeight: {
        500: 500,
        400: 400,
        600: 600,
        700: 700,
      },
      height: {
        100: "100%",
        "53px": "53px",
      },
      margin: {
        "12px": "12px",
        "20px": "20px",
        "16px": "16px",
      },
      borderWidth: {
        1: "0.75px",
      },
      flexBasis: {
        60: "60%",
        40: "40%",
      },
      padding: {
        "30%": "30%",
        "10px": "10px",
        "12px": "12px",
        "16px": "16px",
      },
      fontSize: {
        "20px": "20px",
        "16px": "16px",
      },
      width: {
        "290px": "290px",
        "1.75rem": "1.75rem",
        "100%": "100%",
        "2.3rem": "2.3rem",
        "70%": "70%",
      },
      backgroundColor: {
        "button-hover": "rgba(15,20,25,0.1)",
        subscribe: "#0f1419",
        "post-btn": "rgb(29, 155, 240)",
        "post-btn-hover": "rgb(26, 140, 216)",
        "trend-bg": "#EFF3F4",
        "subscribe-hover": "#272c30",
      },
      height: {
        "1.75rem": "1.75rem",
        "2.3rem": "2.3rem",
      },
      minHeight: {
        "48px": "48px",
      },
      minWidth: {
        "52px": "52px",
      },
      maxWidth: {
        "52px": "52px",
      },
      colors: {
        "grey-dull": "#536471",
      },
    },
  },
  plugins: [],
};
