const FollowItem: React.FC<{ image: string; name: string; id: string }> = ({
  image,
  name,
  id,
}) => {
  return (
    <div className="flex flex-row items-center my-4">
      <img className=" mr-4 h-2.3rem w-2.3rem" src={image} alt="" />
      <div className="flex flex-col">
        <p>{name}</p>
        <p className="text-grey-dull text-sm">{id}</p>
      </div>
      <button className="rounded-full font-700 text-white w-20 py-1 ml-auto px-2 bg-subscribe hover:bg-subscribe-hover">
        Follow
      </button>
    </div>
  );
};

export default FollowItem;
