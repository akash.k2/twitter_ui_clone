import Feeds from "./Feeds";
import Trending from "./Trending";

const MainContainer = () => {
  return (
    <div className="flex flex-row">
      <div className="basis-60">
        <Feeds />
      </div>
      <div className="basis-40 sticky top-0 h-screen overflow-y-scroll scrollbar-hidden">
        <Trending />
      </div>
    </div>
  );
};

export default MainContainer;
