import FollowItem from "./FollowItem";
import TrendingItems from "./TrendingItems";
import Logo from "../assets/twitter_profile.png";

const Trending = () => {
  return (
    <div className="ml- flex flex-col ml-12px">
      <input
        className="mb-12px placeholder-black bg-trend-bg rounded-3xl mt-2 w-80 appearance-none outline-none p-10px"
        placeholder="Search"
        type="text"
      ></input>
      <div className="bg-trend-bg p-4 rounded-3xl w-80 mb-12px">
        <p className="font-700 text-xl">Subscribe to Premium</p>
        <p className="mt-3 font-500">
          Subscribe to unlock new features and if eligible, receive a share of
          ads revenue.
        </p>
        <button className="rounded-full font-700 mt-2 text-white w-36 py-2 px-2 bg-subscribe hover:bg-subscribe-hover">
          Subscribe
        </button>
      </div>
      <div className="bg-trend-bg p-4 mt rounded-3xl w-80 mb-12px">
        <p className="font-700 text-lg pb-2">What's happening</p>

        <TrendingItems
          field="Education"
          topic="Premier League"
          postsCount="7,092"
        />
        <TrendingItems
          field="Education"
          topic="Premier League"
          postsCount="7,092"
        />
        <TrendingItems
          field="Education"
          topic="Premier League"
          postsCount="7,092"
        />
        <p className="text-blue-400">Show more</p>
      </div>
      <div className="bg-trend-bg p-4 mt rounded-3xl w-80 mb-12px">
        <p className="font-700 text-lg pb-2">Who to follow</p>

        <FollowItem image={Logo} name="Virat Kohli" id="@viratkohli" />
        <FollowItem image={Logo} name="Virat Kohli" id="@viratkohli" />
        <FollowItem image={Logo} name="Virat Kohli" id="@viratkohli" />
        <p className="text-blue-400">Show more</p>
      </div>
    </div>
  );
};

export default Trending;
