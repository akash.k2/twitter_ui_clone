const TrendingItems: React.FC<{
  topic: string;
  field: string;
  postsCount: string;
}> = ({ topic, field, postsCount }) => {
  return (
    <div className="my-4">
      <div className="flex flex-row text-xs items-center w-100% justify-between text-grey-dull">
        <p className="mr-1">{field} · Trending</p>
        <p className="text-grey-dull font-600 text-lg">···</p>
      </div>
      <p className="font-500 text-16px">{topic}</p>
      <p className="text-grey-dull text-xs">{postsCount} posts</p>
    </div>
  );
};

export default TrendingItems;
