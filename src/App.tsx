import "./App.css";
import MainContainer from "./Components/MainContainer";
import NavBar from "./Components/NavBar";

function App() {
  return (
    <div className="flex flex-row">
      <div className="sticky top-0 h-screen overflow-y-scroll scrollbar-hidden">
        <NavBar />
      </div>
      <div className="flex-1">
        <MainContainer />
      </div>
    </div>
  );
}

export default App;
